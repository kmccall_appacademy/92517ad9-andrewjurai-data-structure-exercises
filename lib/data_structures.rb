# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr.sort == arr
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowel_number = 0
  vowels = "aeiou"
  str.downcase.each_char { |ch| vowel_number += 1 if vowels.include?(ch) }
  vowel_number
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  devowled = str.chars.map { |ch| ch unless "aeiou".include?(ch.downcase) }
  devowled.join
end


# HARD

# Write a method that returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  non_repeating_length = str.downcase.chars.uniq.length
  original_length = str.chars.length
  return false if non_repeating_length == original_length
  true
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  "(#{arr[0, 3].join}) #{arr[3, 3].join}-#{arr[-4..-1].join}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7

def string_int_to_array(str)
  array_of_str = str.split(",")
  array_of_str.map(&:to_i)
end

def str_range(str)
  string_int_to_array(str).max - string_int_to_array(str).min
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset = 1)
  offset = large_offset_correction(arr, offset)
  if offset > 0
    arr.drop(offset) + arr.take(offset)
  elsif offset < 0
    answer = arr.reverse.drop(offset.abs) + arr.reverse.take(offset.abs)
    answer.reverse
  end
end

def large_offset_correction(arr, offset)
  return offset % arr.length if offset > arr.length
  offset
end
